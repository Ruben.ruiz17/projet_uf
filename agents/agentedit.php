<?php
// Include config file
require_once "../php/config.php";
 
session_start();

if (isset($_SESSION["loggedin"]) === false){
    header("location: agentlogin.php");
    exit;
}
 
$sql = "SELECT * FROM employees WHERE username = :username";
 
$query = $pdo->prepare($sql);
$query->bindParam(":username", $_SESSION["username"], PDO::PARAM_STR);
$query->execute();
$array = $query->fetch(PDO::FETCH_NUM);

$lname = $fname = $email = $phone = $agency = "";

unset($sql);
// Define variables and initialize with empty values
$agency_err = $email_err = $lname_err = $fname_err = $phone_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    
    if(empty(trim($_POST["lname"]))){
        $lname = $array[1];     
    } else {
        $lname = trim($_POST["lname"]);
    }

    if(empty(trim($_POST["agency"]))){
        $agency = $array[6];     
    } else {
        $agency = trim($_POST["agency"]);
    }

    if(empty(trim($_POST["fname"]))){
        $fname = $array[2];  
    } else {
        $fname = trim($_POST["fname"]);
    }

    if(empty(trim($_POST["email"]))){
    $email = $array[5];
    } else{
        // Prepare a select statement
        $sql = "SELECT email FROM employees WHERE email = :email";
        
        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":email", $param_email, PDO::PARAM_STR);
            
            // Set parameters
            $param_email = trim($_POST["email"]);
            
            // Attempt to execute the prepared statement
            if($stmt->execute()){
                if($stmt->rowCount() == 1){
                    $email_err = "Cet e-mail est déjà pris.";
                } else{
                    $email = trim($_POST["email"]);
                }
            } else{
                echo "Oops! une erreur est survenue. veuillez re-essayé après.";
            }

            // Close statement
            unset($stmt);
        }
    }

    if(empty(trim($_POST["phone"]))){
        $phone = $array[4];
    } else{
        // Prepare a select statement
        $sql = "SELECT telephone FROM employees WHERE telephone = :phone";
        
        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":phone", $param_phone, PDO::PARAM_STR);
            
            // Set parameters
            $param_phone = trim($_POST["phone"]);
            
            // Attempt to execute the prepared statement
            if($stmt->execute()){
                if($stmt->rowCount() == 1){
                    $phone = $array[4];
                } else{
                    $phone = trim($_POST["phone"]);
                }
            } else{
                echo "Oops! une erreur est survenue. veuillez re-essayé après.";
            }

            // Close statement
            unset($stmt);
        }
    }



    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
        // Prepare an insert statement
        $sql = "UPDATE employees SET nom = :lname, prenom = :fname, telephone = :phone, email = :email, id_agence = :agence WHERE username = :username";

        $sqlcity = "SELECT id_agence FROM agence WHERE ville LIKE CONCAT('%', :ville, '%')";
        $query = $pdo->prepare($sqlcity);
        $query->bindParam(":ville", $agency);
        $query->execute();
        $agency = $query->fetch(PDO::FETCH_NUM);
         
        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":username", $_SESSION["username"], PDO::PARAM_STR);
            $stmt->bindParam(":lname", $param_lname, PDO::PARAM_STR);
            $stmt->bindParam(":fname", $param_fname, PDO::PARAM_STR);
            $stmt->bindParam(":email", $param_email, PDO::PARAM_STR);
            $stmt->bindParam(":phone", $param_phone, PDO::PARAM_STR);
            $stmt->bindParam(":agence", $param_agency);

            
            // Set parameters
            $param_lname = $lname;
            $param_fname = $fname;
            $param_email = $email;
            $param_phone = $phone;
            $param_agency = $agency[0];

            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // Redirect to login page
                header("location: agentaccount.php");
            } else{
                echo "Une erreur est survenue. veuillez re-essayé après.";
            }

            // Close statement
            unset($stmt);
        }
    }
    
    // Close connection
    unset($pdo);
}
?>  
 
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <meta charset="UTF-8">
    <title>Modifier</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
        <nav class="navmenu">
            <?php 
            if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
                echo('<a href="agentlogout.php">Déconnexion</a>');
                echo('<a href="agentaccount.php">Mon Compte</a>');
            } else {
                echo('<a href="agentlogin.php">Connexion</a>');
            }
            ?>

        </nav>
    </header>
    <div class="background"></div>
    <div class="wrapper">
        <h2>Modifier votre compte</h2>
        <p>Veuillez remplir ce formulaire pour modifier votre compte.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">   
            <div class="form-group <?php echo (!empty($lname_err)) ? 'has-error' : ''; ?>">
                <label>Nom</label>
                <input type="text" name="lname" class="form-control" value="<?php echo $lname; ?>">
                <span class="help-block"><?php echo $lname_err; ?></span>
            </div>  
            <div class="form-group <?php echo (!empty($fname_err)) ? 'has-error' : ''; ?>">
                <label>Prénom</label>
                <input type="text" name="fname" class="form-control" value="<?php echo $fname; ?>">
                <span class="help-block"><?php echo $fname_err; ?></span>
            </div>   
            <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                <label>E-mail</label>
                <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                <span class="help-block"><?php echo $email_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($phone_err)) ? 'has-error' : ''; ?>">
                <label>Numéro de téléphone</label>
                <input type="text" name="phone" class="form-control" value="<?php echo $phone; ?>">
                <span class="help-block"><?php echo $phone_err; ?></span>
            </div> 
            <div class="form-group <?php echo (!empty($agency_err)) ? 'has-error' : ''; ?>">
                <label>Votre Agence</label>
                <input type="text" name="agency" class="form-control" value="<?php echo $agency; ?>">
                <span class="help-block"><?php echo $agency_err; ?></span>
            </div>     
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Valider les modifications">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            
        </form>
        <a href="agentaccount.php">Retour au compte.</a>
    </div>    
</body>
</html>