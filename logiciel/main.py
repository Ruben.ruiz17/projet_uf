import PySimpleGUI as sg
import mysql.connector

"""Docstring d'une ligne décrivant brièvement ce que fait le programme.

Usage:
======
    Ce code sert a ajouter,lire,supprimer et modifier une annonce sur une base de donnée

"""

__authors__ = ("Ruben RUIZ")
__contact__ = ("ruben.ruiz@ynov.com")
__version__ = "1.5.0"
__copyright__ = "copyleft"
__date__ = "02/06/2020"


def annonces():
    layout = [[  # Creating the layout.
        sg.Text(
            "Merci d'entrer les informations concernant le bien.",
            font='Any 15',
            justification='center')
    ], [sg.Text('_' * 100, size=(65, 1))], [
        sg.Text(
            'Informations du Bien',
            font=('Helvetica', 15),
            justification='left')
    ], [sg.Text('Prix', size=(15, 1)),
        sg.InputText()], [sg.Text('Addresse', size=(15, 1)),
                          sg.InputText()],
              [sg.Text('Ville', size=(15, 1)),
               sg.InputText()], [
                   sg.Text('Code Postale', size=(15, 1)),
                   sg.InputText()
               ], [sg.Text('Superficie', size=(15, 1)),
                   sg.InputText()], [
                       sg.Text('Type de bien', size=(15, 1)),
                       sg.InputText()
                   ], [
                       sg.Text('Nombre de pièce', size=(15, 1)),
                       sg.Spin(
                           values=[i for i in range(1, 21)],
                           initial_value=1,
                           size=(6, 1))
                   ], [sg.Text('Description', size=(15, 1)),
                       sg.InputText()], [sg.Text('_' * 100, size=(65, 1))], [
                           sg.Text(
                               'Informations Vendeur',
                               font=('Helvetica', 15),
                               justification='left')
                       ], [
                           sg.Text('E-mail', size=(15, 1)),
                           sg.InputText(key='email')
                       ], [sg.Button('Ajouter'),
                           sg.Button('Exit')]]

    # Create the Window
    window = sg.Window("Ajout d'annonce", layout, finalize=True)

    def progressbar():
        for i in range(1000):
            sg.OneLineProgressMeter('Uploading', i + 1, 1000, 'mymeter')

    while True:
        event, values = window.read()
        if event in (None, 'Exit'):
            break
        elif event in 'Ajouter':
            progressbar()
            # connecting to the database
            dataBase = mysql.connector.connect(
                host="localhost", user="root", passwd="", database="uf_b1b")

            # preparing a cursor object
            cursorObject = dataBase.cursor()
            cursorObject.execute("ALTER TABLE biens AUTO_INCREMENT = 0")
            dataBase.commit()
            idusers = 0

            def user():
                # selecting query
                cursorObject.execute(
                    "SELECT id_users FROM users WHERE email='%s'" % values['email'])
                myresult = cursorObject.fetchall()
                for row in myresult:
                    idusers = row[0]
                return idusers

            sql = "INSERT INTO biens (id_biens,prix_vente, adresse,ville,code_postale,superficie,type_appart,nb_piece,description,id_users) VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s,%s)"
            val = (cursorObject.lastrowid, values[0], values[1], values[2],
                   values[3], values[4], values[5], values[6], values[7], user())

            cursorObject.execute(sql, val)
            dataBase.commit()

            sg.SystemTray.notify('Ajout Reussi!',
                                 'Le bien a été ajouté dans la base de donnée')
            # disconnecting from server
            dataBase.close()
    window.close()


def table_example():
    header_list = []
    dataBase = mysql.connector.connect(
        host="localhost", user="root", passwd="", database="uf_b1b")
    mycursor = dataBase.cursor()
    mycursor.execute(
        'SELECT `id_biens`,`superficie`,`description`,`prix_vente`,`date_ajout`,`type_appart`,`nb_piece`,`ville`,`code_postale`,`adresse` FROM `biens` ORDER BY `id_biens` ASC'
    )
    reader = mycursor.fetchall()  # Fetching the result of sql query.

    header_list = [
        'id de biens', 'superficie', 'description', 'prix de vente',
        'date d\'ajout', 'type de biens', 'nombre de piece', 'ville',
        'code postale', 'adresse'
    ]
    layout = [[
        sg.Text("Voici les annonces", font='Any 15', justification='center')
    ], [
        sg.Table(
            values=reader,
            headings=header_list,
            max_col_width=25,
            auto_size_columns=True,
            justification='right',
            key=('-IN-'))
    ], [sg.Button("Delete"),
        sg.Button("Modifier"),
        sg.Button("Exit")]]

    window = sg.Window('Supression d\'annonces', layout, finalize=True)
    window2_active = False
    cursorObject = dataBase.cursor()
    cursorObject.execute("SELECT id_biens FROM biens ORDER BY `id_biens` ASC")
    myresult = cursorObject.fetchall()
    while True:
        event1, values = window.read()
        if event1 in (None, 'Exit'):
            break
        if not window2_active and event1 == 'Delete':
            dataBase = mysql.connector.connect(
                host="localhost", user="root", passwd="", database="uf_b1b")
            mycursor = dataBase.cursor()
            a = values['-IN-']     
            for i in range(len(a)):
                myresult[i] = tuple(myresult[i])
                listToStr = ' '.join(map(str, myresult[a[i]]))
            sql = ("DELETE FROM biens WHERE id_biens =%s")
            mycursor.execute(sql, (listToStr, ))

            sg.SystemTray.notify(
                'Supression Reussi!',
                'Le bien disparaîtra du tableau au prochain redémarrage de l\'application'
            )
        if not window2_active and event1 == 'Modifier':
            window2_active = True
            window.hide()
            layout2 = [[  # Creating the layout.
                sg.Text(
                    "Merci d'entrer les informations concernant le bien.",
                    font='Any 15',
                    justification='center')
            ], [sg.Text('_' * 100, size=(65, 1))], [
                sg.Text(
                    'Informations du Bien',
                    font=('Helvetica', 15),
                    justification='left')
            ], [sg.Text('Prix', size=(15, 1)),
                sg.InputText(key='prix')], [
                    sg.Text('Addresse', size=(15, 1)),
                    sg.InputText(key='adresse')
                ], [sg.Text('Ville', size=(15, 1)),
                    sg.InputText(key='ville')], [
                        sg.Text('Code Postale', size=(15, 1)),
                        sg.InputText(key='CP')
                    ], [
                        sg.Text('Superficie', size=(15, 1)),
                        sg.InputText(key='superficie')
                    ], [
                        sg.Text('Type de bien', size=(15, 1)),
                        sg.InputText(key='type')
                    ], [
                        sg.Text('Nombre de pièce', size=(15, 1)),
                        sg.Spin(
                            key='piece',
                            values=[i for i in range(1, 21)],
                            initial_value=1,
                            size=(6, 1))
                    ], [
                        sg.Text('Description', size=(15, 1)),
                        sg.InputText(key='desc')
                    ], [sg.Button('Modifier'),
                        sg.Button('Exit')]]
            window2 = sg.Window('Modification du bien', layout2)
            while True:
                ev2, values2 = window2.read()
                if ev2 in (None, 'Exit'):
                    window2_active = False
                    window2.close()
                    window.un_hide()
                    break
                elif ev2 in 'Modifier':
                    dataBase = mysql.connector.connect(
                        host="localhost",
                        user="root",
                        passwd="",
                        database="uf_b1b")  # Connecting to the database with mysql.connector.
                    mycursor = dataBase.cursor()
                    a = values['-IN-']
                    for i in range(len(a)):
                        myresult[i] = tuple(myresult[i])
                        listToStr = ' '.join(map(str, myresult[a[i]]))
                    sql = (
                        "UPDATE biens SET id_biens=%s, superficie=%s,description=%s,prix_vente=%s,type_appart=%s,nb_piece=%s,ville=%s,code_postale=%s,adresse=%s WHERE id_biens =%s"
                    )
                    val = (listToStr, values2['superficie'], values2['desc'],
                           values2['prix'], values2['type'], values2['piece'],
                           values2['ville'], values2['CP'], values2['adresse'],
                           listToStr)
                    mycursor.execute(sql, val)
                    dataBase.commit()
                    sg.SystemTray.notify(
                        'Modification Reussi!',
                        'La modification dans le logiciel sera affichée au prochain redémarrage de celci.'
                    )
        if window2_active:
            event2 = window2.read()
            if event2 in (sg.WIN_CLOSED, 'Exit'):
                window2_active = False
                window2.close()
                window.un_hide()

    window.close()


def main():  # The main function.
    layout = [[
        sg.Text(
            "Bonjour, Merci de choisir ce que vous voulez faire : ",
            font='Any 15',
            justification='center'),
    ], [
        sg.Button('Ajout d\'annonces'),
        sg.Button('Supression/Modification d\'annonce'),
        sg.Button('Exit')
    ]]

    # Create the Window
    window = sg.Window("Stephi Place", layout, finalize=True)
    while True:
        event, values = window.read()
        if event in (None, 'Exit'):
            break
        if event == 'Ajout d\'annonces':
            annonces()  # Call the function annonces().
        if event == 'Supression/Modification d\'annonce':
            table_example()  # Call the function table_exemple().

    window.close()


if __name__ == "__main__":
    main()  # The main entry of the program.
