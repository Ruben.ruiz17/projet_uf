<?php
/* Recuperation des paramètres pour la base de donnée */
$db_params = parse_ini_file('../db.ini');

/* Connection a la base de donnée */
try{
	$pdo = new PDO($db_params['url'], $db_params['user'], $db_params['pass']);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    die("ERROR: Could not connect. " . $e->getMessage());
}

?>