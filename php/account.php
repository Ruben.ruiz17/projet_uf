<?php

session_start();

if (isset($_SESSION["loggedin"]) === false){
    header("location: ../index.php");
    exit;
}

require_once "config.php";

$sql = "SELECT * FROM users WHERE username = :username";
 
$query = $pdo->prepare($sql);
$query->bindParam(":username", $_SESSION["username"], PDO::PARAM_STR);
$query->execute();
$array = $query->fetch(PDO::FETCH_NUM);
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <meta charset="UTF-8">
    <title>Compte</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/accountstyle.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
		<header>
			<nav class="navmenu">
				<a href="../index.php">Accueil</a>
			    <a href="search.php">Annonces</a>
			    <a href="agencysearch.php">Agences</a>
			    <?php 
				if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
	    			echo('<a href="logout.php">Déconnexion</a>');
	    			echo('<a href="account.php">Mon Compte</a>');
	    		} else {
	    			echo('<a href="login.php">Connexion</a>');
	    		}
			    ?>

			</nav>
		</header>
	<div class="background"></div>
	<div class="container emp-profile">
	                <div class="row">
	                    <div class="col-md-4">
	                        <div class="profile-img">
	                            <img src="../img/profilepic.jpg" alt=""/>
	                        </div>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="profile-head">
	                                    <h5>
	                                        <?php echo($array[1]); ?>
	                                    </h5>
	                                    <h6>
	                                        <?php echo($array[5] . " " . $array[4]); ?>
	                                    </h6>
	                            <ul class="nav nav-tabs" id="myTab" role="tablist">
	                                <li class="nav-item">
	                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Informations</a>
	                                </li>
	                            </ul>
	                        </div>
	                    </div>
	                    <div class="col-md-2">
	                    	<form method="POST" action="edit.php">
	                        <a href="edit.php" class="btn btn-info" name="btnAddMore" >Modifier le profil</a>
	                    	</form>

	                    		<form method="POST" action="favorites.php">
	                    	    <a href="favorites.php" class="btn btn-warning" name="btnAddMore" >Accéder aux favoris</a>
	                    		</form>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-md-4">
	                    </div>
	                    <div class="col-md-8">
	                                        <div class="row">
	                                            <div class="col-md-6">
	                                                <label>Nom d'Utilisateur</label>
	                                            </div>
	                                            <div class="col-md-6">
	                                                <p><?php echo($array[1]); ?></p>
	                                            </div>
	                                        </div>
	                                        <div class="row">
	                                            <div class="col-md-6">
	                                                <label>Nom et prénom</label>
	                                            </div>
	                                            <div class="col-md-6">
	                                                <p> <?php echo($array[5] . " " . $array[4]); ?></p>
	                                            </div>
	                                        </div>
	                                        <div class="row">
	                                            <div class="col-md-6">
	                                                <label>Email</label>
	                                            </div>
	                                            <div class="col-md-6">
	                                                <p><?php echo($array[7]); ?></p>
	                                            </div>
	                                        </div>
	                                        <div class="row">
	                                            <div class="col-md-6">
	                                                <label>Téléphone</label>
	                                            </div>
	                                            <div class="col-md-6">
	                                                <p><?php echo($array[6]); ?></p>
	                                            </div>
	                                        </div>
	                        </div>
	                    </div>         
	        </div>
</body>