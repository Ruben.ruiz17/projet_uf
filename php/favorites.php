<?php

session_start();

if (isset($_SESSION["loggedin"]) === false){
    header("location: ../index.php");
    exit;
}

require_once "config.php";

$sql = "SELECT * FROM favori WHERE id_users = :username";
 
$query = $pdo->prepare($sql);
$query->bindParam(":username", $_SESSION["id"], PDO::PARAM_STR);
$query->execute();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Favoris</title>
	<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/accountstyle.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
		<header>
			<nav class="navmenu">
				<a href="../index.php">Accueil</a>
			    <a href="search.php">Annonces</a>
			    <a href="agencysearch.php">Agences</a>
			    <?php 
				if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
	    			echo('<a href="logout.php">Déconnexion</a>');
	    			echo('<a href="account.php">Mon Compte</a>');
	    		} else {
	    			echo('<a href="login.php">Connexion</a>');
	    		}
			    ?>

			</nav>
		</header>
	<div class="background"></div>
	<div class="container emp-profile">
	                <div class="row">
	                    <div class="col-md-4">
	                        <div class="profile-img">
	                            <img src="../img/profilepic.jpg" alt=""/>
	                        </div>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="profile-head">
	                                    <h5>
	                                        Favoris de : 
	                                    </h5>
	                                    <h6>
	                                        <?php echo($_SESSION["username"]) ?>
	                                    </h6>
	                            <ul class="nav nav-tabs" id="myTab" role="tablist">
	                                <li class="nav-item">
	                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Liste</a>
	                                </li>
	                            </ul>
	                        </div>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-md-4">
	                    </div>
	                    <div class="col-md-8">
	                    	<?php
	                    	$array = 1;
	                    	$i = 0;
	                    		while($array == true) {
	                    			
	                    			$i++;
	                    			
	                    			$array = $query->fetch(PDO::FETCH_NUM);
	                    			if ($i == 1 AND $array == false) {
	                    				   		echo("Vous n'avez pas de favoris.");
	                    				   	}
	                    			if($array == true){
	                    			echo( "
	                    			<div class='row'>
	                    			    <div class='col-md-6'>
	                    			        <label>Favori #" . $array[1] . "</label>
	                    			        <form action='delfav.php' method='POST' class='buttonform'>
	                    			        <button type='submit' class='btn btn-danger btn-lg' name='delid' value='".$array[1]."'>Supprimer le Favori #</a>
	                    			        </form>
	                    			    </div>
	                    			    <div class='col-md-6'>
	                    			        <form action='annonce.php' method='POST' class='buttonform'>
	                    			        <button type='submit' class='btn btn-info btn-lg' name='id' value='".$array[2]."'>Voir le bien #" . $array[2] ."</a>
	                    			        </form>
	                    			    </div>
	                    			</div>
	                    			"
	                    			 );
	                    				   	} 
	                    			}
	                    		
	                    	?>
	                                        
	                                        
	                        </div>
	                    </div>         
	        </div>
</body>