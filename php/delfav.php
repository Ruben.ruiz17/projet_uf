<?php 

require_once "config.php";

$id = $_POST["delid"];
$sql = "DELETE FROM favori WHERE id_favoris = :id";
$query = $pdo->prepare($sql);
$query->bindParam(":id", $id, PDO::PARAM_STR);
$query->execute();
header("location:favorites.php");

?>