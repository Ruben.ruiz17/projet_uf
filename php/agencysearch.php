<?php
// ville minprice maxprice minsurface maxsurface radius appartment house studio

session_start();

require_once "config.php";

if(isset($_POST["ville"])){

$ville = $_POST["ville"];

$sql = "SELECT * FROM agence WHERE ville LIKE CONCAT('%', :ville, '%')";
 
$query = $pdo->prepare($sql);
$query->bindParam(":ville", $ville, PDO::PARAM_STR);
$query->execute();

}


?>


<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	<meta name="description" content="Site Web Stephi Place Onglet permettant de rechercher des agences dans toute la france." />
	<title>Recherche d'agences !</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/style2.css">
</head>
<body>
		<header>
			<nav class="navmenu">
				<a href="../index.php">Accueil</a>
			    <a href="search.php">Annonces</a>
			    <a href="agencysearch.php">Agences</a>
			    <?php 
				if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
	    			echo('<a href="logout.php">Déconnexion</a>');
	    			echo('<a href="account.php">Mon Compte</a>');
	    		} else {
	    			echo('<a href="login.php">Connexion</a>');
	    		}
			    ?>

			</nav>
		</header>
	<div class="background"></div>
	<form class="form-inline mr-auto" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?> 	">
			<div class="positionning"><label>Ville</label><input class="form-control mr-sm-2" type="text" placeholder="Ville" aria-label="Ville" name="ville"></div>
			</div>
			<div class="positionning">
			<label>Vous avez terminé?</label>
			<button type="submit" class="btn btn-success">Envoyer !</button>
			</div>
</form>

	<div class="contain">

		<?php
		if(isset($_POST["ville"])){
		$i = 0;
		$array=1;
			while($i<16) {
				$i++;
				$array = $query->fetch(PDO::FETCH_NUM);
				if($array == true){

				echo(
					"
					<div class='card text-center' style='width: 18rem;'>
					  <img class='card-img-top' src='../img/maison.jpg' alt='Card image cap'>
					  <div class='card-body'>
					    <h5 class='card-title'> Agence #" . $array[0] . "</h5>
					    <p class='card-text'>" . $array[1] ."</p>
					    <div class='forcecolumn'>
					    <label>Ville : " . $array[2] . " </label>
					    <label>Numéro de téléphone : " . $array[3] . " </label>
					    <label>Adresse : " . $array[4] . "</label>
						</div>
						<form action='agency.php' method='POST' class='buttonform'>
					    <button type='submit' class='btn btn-primary' name='id' value='".$array[0]."'>Voir l'annonce</a>
					    </form>
					    </div>
					    </div>");

				}
			}
		}
		?>
	</div>

</body>
</html>