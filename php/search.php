<?php
// ville minprice maxprice minsurface maxsurface radius appartment house studio

session_start();

require_once "config.php";

if(isset($_POST["ville"]) AND isset($_POST["minprice"]) AND isset($_POST["maxprice"]) AND isset($_POST["minsurface"]) AND isset($_POST["maxsurface"]) AND isset($_POST["roomamount"])){

$ville = $_POST["ville"];
$minprice = $_POST["minprice"];
$maxprice = $_POST["maxprice"];
$minsurface = $_POST["minsurface"];
$maxsurface = $_POST["maxsurface"];
$roomamount = $_POST["roomamount"];

$immotype = $_POST["immotype"];

$trigger = "CREATE TRIGGER MysqlTrigger BEFORE SELECT ON biens SET NEW.ville=LOWER(NEW.ville);";

$sql = "SELECT ville, id_biens, prix_vente, superficie, type_appart, nb_piece, description FROM biens WHERE ville LIKE CONCAT('%', :ville, '%') AND nb_piece = :nb_piece AND type_appart = :type_appart AND prix_vente BETWEEN :minprice AND :maxprice AND superficie BETWEEN :minsuperficie AND :maxsuperficie";

$query = $pdo->prepare($sql);
$query->bindParam(":ville", $ville, PDO::PARAM_STR);
$query->bindParam(":nb_piece", $roomamount, PDO::PARAM_STR);

$query->bindParam(":type_appart", $immotype, PDO::PARAM_STR);

$query->bindParam(":minprice", $minprice, PDO::PARAM_STR);
$query->bindParam(":maxprice", $maxprice, PDO::PARAM_STR);
$query->bindParam(":minsuperficie", $minsurface, PDO::PARAM_STR);
$query->bindParam(":maxsuperficie", $maxsurface, PDO::PARAM_STR);
$query->execute();

}

if(isset($_POST["favbutton"])) {
	$favsql = "INSERT INTO favori (id_users, id_biens) VALUES (:user, :bien);";
	$favquery = $pdo->prepare($favsql);
	$favquery -> bindParam(":user", $_SESSION["id"]);
	$favquery -> bindParam(":bien", $_POST["favbutton"]);
	$favquery -> execute();
}

?>


<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
	<title>Recherche !</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/style2.css">
</head>
<body>
		<header>
			<nav class="navmenu">
				<a href="../index.php">Accueil</a>
			    <a href="search.php">Annonces</a>
			    <a href="agencysearch.php">Agences</a>
			    <?php 
				if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
	    			echo('<a href="logout.php">Déconnexion</a>');
	    			echo('<a href="account.php">Mon Compte</a>');
	    		} else {
	    			echo('<a href="login.php">Connexion</a>');
	    		}
			    ?>

			</nav>
		</header>
	<div class="background"></div>
	<form class="form-inline mr-auto" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?> 	">
			<div class="positionning"><label>Ville</label><input class="form-control mr-sm-2" type="text" placeholder="Ville" aria-label="Ville" name="ville"></div>
			<div class="positionning"><label>Prix Minimum</label><input class="form-control mr-sm-2" type="text" placeholder="Prix_min" aria-label="Prix_min" name="minprice"></div>
			<div class="positionning"><label>Prix Maximum</label><input class="form-control mr-sm-2" type="text" placeholder="Prix_max" aria-label="Prix_max" name="maxprice"></div>
			<div class="positionning"><label>Surface Minimum</label><input class="form-control mr-sm-2" type="text" placeholder="Surface_min" aria-label="Surface_min" name="minsurface"></div>
			<div class="positionning"><label>Surface Maximum</label><input class="form-control mr-sm-2" type="text" placeholder="Surface_max" aria-label="Surface_max" name="maxsurface"></div>
			<div class="positionning"><label>Nombre de pièces</label><input class="form-control mr-sm-2" type="text" placeholder="Nombre pièces" aria-label="Nombre pièces" name="roomamount"></div>
			<div class="positionning">
				<label>Type d'Appartement</label>
				<select name="immotype" class="form-control">
				  <option value="appartement">Appartement</option>
				  <option value="maison">Maison</option>
				  <option value="studio">Studio</option>
				</select>
			</div>
			<div class="positionning">
				<label>Particularités</label>
				<div class="dropdown">
				  <button class="dropbtn">Choisissez !</button>
				  <div class="dropdown-content">
				  	<div class="dropdownitem">
				    	<input type="checkbox" value="piscine" value="Piscine">
				    	<label for="piscine">Piscine</label>
				    </div>
				    <div class="dropdownitem">
				    	<input type="checkbox" value="Balcon" value="Balcon">
				    	<label for="Balcon">Balcon</label>
				     </div>
			    	<div class="dropdownitem">
			      		<input type="checkbox" value="Cave" value="Cave">
			      		<label for="Cave">Cave</label>
			      	</div>
				  </div>
				</div> 
			</div>
			<div class="positionning">
			<label>Vous avez terminé?</label>
			<button type="submit" class="btn btn-success">Envoyer !</button>
			</div>
</form>

	<div class="contain">

		<?php
		if(isset($_POST["ville"])){
		$i = 0;
		$array=1;
			while($i<16) {
				$i++;
				$array = $query->fetch(PDO::FETCH_NUM);
				if($array == true){
					$verifsql = "SELECT * FROM favori WHERE id_users = :user AND id_biens = :bien";
					$verifquery = $pdo->prepare($verifsql);
					$verifquery -> bindParam(":user", $_SESSION["id"]);
					$verifquery -> bindParam(":bien", $array[1]);
					$verifquery -> execute();
					$verifarray = $verifquery->fetch(PDO::FETCH_NUM);

				echo(
					"
					<div class='card text-center' style='width: 18rem;'>
					  <img class='card-img-top' src='../img/maison.jpg' alt='Card image cap'>
					  <div class='card-body'>
					    <h5 class='card-title'> Annonce #" . $array[1] . "</h5>
					    <p class='card-text'>" . $array[6] ."</p>
					    <div class='forcecolumn'>
					    <label>Prix : " . $array[2] . "€ </label>
					    <label>Surface : " . $array[3] . "m² </label>
					    <label>Ville : " . $array[0] . "</label>
					    <label>Type : " . $array[4]."</label>
						</div>
						<form action='annonce.php' method='POST' class='buttonform'>
					    <button type='submit' class='btn btn-primary' name='id' value='".$array[1]."'>Voir l'annonce</a>
					    </form>");

					   	if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true AND $verifarray == false) {
					   		echo("<form action='" . $_SERVER['PHP_SELF'] . "' method='POST' class='buttonform'>
					    <button type='submit' class='btn btn-warning' name='favbutton' value='".$array[1]."'>Ajout Favori</a>
					    </form>
					  </div>
					</div>");
					   	} else {
					   		echo("</div></div>");
					   	}
				}
			}
		}
		?>
	</div>

</body>
</html>