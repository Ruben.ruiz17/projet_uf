
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" type="text/css" href="css/indexstyle.css" />
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<title>Stephi Places - Accueil</title>
	<meta name="description" content="Site web crée pour un projet UF étudiant de bachelor, il y contient la page web d'une agence immobilière" />
	<script async src="js/indexscript.js"></script>
	<link rel="stylesheet" href="css/flickity.css">
	<link rel="stylesheet" href="css/normalize.css">
	<script async src="js/flickity.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge" />
	<meta name="author" content="Ruben RUIZ, Romuald Parmentier" />
	<noscript>
		<link rel="stylesheet" type="text/css" href="css/noscriptindex.css" />
	</noscript>
</head>
<body onload="init();">
    <div class="background"></div>
		<header>
			<nav class="navmenu">
				<a href="index.php">Accueil</a>
			    <a href="php/search.php">Annonces</a>
			    <a href="php/agencysearch.php">Agences</a>
			    <?php
			    session_start(); 
				if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
	    			echo('<a href="php/logout.php">Déconnexion</a>');
	    			echo('<a href="php/account.php">Mon Compte</a>');
	    		} else {
	    			echo('<a href="php/login.php">Connexion</a>');
	    		}
			    ?>

			</nav>
		</header>
	<main>
	<div id="maincontent">
		<img src="img/agent.jpg" alt="Agent Immobilier" id = "agentpic" />
		<div id=titletext>
			<h1>Stephi Places Immobilier</h1>
			<p>

			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? Addidisti ad extremum etiam indoctum fuisse. Duo Reges: constructio interrete. An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit?

			Quod autem ratione actum est, id officium appellamus. Plane idem, inquit, et maxima quidem, qua fieri nulla maior potest. Ratio quidem vestra sic cogit. Quantum Aristoxeni ingenium consumptum videmus in musicis? Quae duo sunt, unum facit. Sed quia studebat laudi et dignitati, multum in virtute processerat. An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit? Quare conare, quaeso.

			</p>
		</div>
		<div id="histogramcontainer">
			<h2>Pourquoi Stephi et non une autre agence !</h2>
			<canvas id="histogram"></canvas>
			<noscript><div><img src="img/histogram.png" alt="histogram"></div></noscript>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non est igitur summum malum dolor. Quare conare, quaeso. Quo modo autem optimum, si bonum praeterea nullum est? Duo Reges: constructio interrete. Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Nullus est igitur cuiusquam dies natalis.</p>
		</div>
	</div>
	<div id="carouselcontainer">
		<div class="carousel js-flickity" data-flickity-options='{ "wrapAround": true }'>
			<div class="carousel-cell">
			  	<img src="img/agent.jpg" alt="immoagent"/>
			  	<p class="carouseltxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat.</p>
			</div>
			<div class="carousel-cell">
			  	<img src="img/agent.jpg" alt="immoagent"/>
			  	<p class="carouseltxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat.</p>
			</div>
			<div class="carousel-cell">
			  	<img src="img/agent.jpg" alt="immoagent"/>
			  	<p class="carouseltxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat.</p>
			</div>
			<div class="carousel-cell">
			  	<img src="img/agent.jpg" alt="immoagent"/>
			  	<p class="carouseltxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat.</p>
			</div>
			<div class="carousel-cell">
			  	<img src="img/agent.jpg" alt="immoagent"/>
			  	<p class="carouseltxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat.</p>
			</div>
			<div class="carousel-cell">
			  	<img src="img/agent.jpg" alt="immoagent"/>
			  	<p class="carouseltxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat.</p>
			</div>
			<div class="carousel-cell">
			  	<img src="img/agent.jpg" alt="immoagent"/>
			  	<p class="carouseltxt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			  	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			  	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			  	consequat.</p>
			</div>
		</div>
	</div>
	<noscript>
		<div id=carousel2container>
			<div class="carousel2">
			    <div class="carousel2-inner">
			        <input class="carousel2-open" type="radio" id="carousel2-1" name="carousel2" hidden="" checked="checked">
			        <div class="carousel2-item">
			            <img src="img/agent.jpg" alt="immoagent"/>
			            <p class="carousel2txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			            consequat.</p>
			        </div>
			        <input class="carousel2-open" type="radio" id="carousel2-2" name="carousel2" hidden="">
			        <div class="carousel2-item">
			            <img src="img/agent.jpg" alt="immoagent"/>
			            <p class="carousel2txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			            consequat.</p>
			        </div>
			        <input class="carousel2-open" type="radio" id="carousel2-3" name="carousel2" hidden="">
			        <div class="carousel2-item">
			            <img src="img/agent.jpg" alt="immoagent"/>
			            <p class="carousel2txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			            consequat.</p>
			        </div>
			        <label for="carousel2-3" class="carousel2-control prev control-1">‹</label>
			        <label for="carousel2-2" class="carousel2-control next control-1">›</label>
			        <label for="carousel2-1" class="carousel2-control prev control-2">‹</label>
			        <label for="carousel2-3" class="carousel2-control next control-2">›</label>
			        <label for="carousel2-2" class="carousel2-control prev control-3">‹</label>
			        <label for="carousel2-1" class="carousel2-control next control-3">›</label>
			        <ol class="carousel2-indicators">
			            <li>
			                <label for="carousel2-1" class="carousel2-bullet">•</label>
			            </li>
			            <li>
			                <label for="carousel2-2" class="carousel2-bullet">•</label>
			            </li>
			            <li>
			                <label for="carousel2-3" class="carousel2-bullet">•</label>
			            </li>
			        </ol>
			    </div>
			</div>
		</div>
	</noscript>

	<footer id="CGU">
		<div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		<a href="agents/agentlogin.php">Vous êtes agents ?</a>
	</footer>
		
	</main>

</body>
</html>